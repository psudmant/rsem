RSEM-1.2.19-patch
=================

A simple patch for RSEM to prevent overflow of the "insertL" 
signed short in PairedEndHit.h when a read with a very large 
TLEN value is encountered

see user group discussion for [details](https://groups.google.com/forum/#!topic/rsem-users/aBYjw1utZl4)

swap out this _PairedEndHit.h_ with the the original and compile as normal
